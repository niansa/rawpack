/*
    rawpack
    Copyright (C) 2020 niansa

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libgen.h>
#include <string.h>
#include "librawpack.h"
#include "termmagic.h"


int main(int argc, char **argv) {
        EL_PREPARE();
	FILE *infile;
	FILE *outfile;
	unsigned int entrynum;
	unsigned int entrynum_procced = 0;
	char **unpack_selection = NULL;
	// Check args
	if (argc < 2) {
		ERROR("Bad usage");
		printf("Usage: %s <input> [files to unpack ...]\n", argv[0]);
		return EXIT_FAILURE;
	}
	if (argc > 2) {
		unpack_selection = argv + 2;
	}
	// Open input file
	LOG("Opening output file...");
	infile = fopen(argv[1], "rb");
	if (!infile) {
		perror(argv[1]);
		return EXIT_FAILURE;
	}
	// Check input file
	LOG("Checking file header...");
	if (!rawpack_checkheader(infile)) {
		ERROR("File has invalid header");
		return EXIT_FAILURE;
	}
	// Get amount of entries
	LOG("Getting amount of entries in file...");
	entrynum = rawpack_readentrynum(infile);
	LOG("Entries: %u", entrynum);
	// Iterate through input files
	struct rawpack_entry thisentry;
	while ((entrynum_procced++) != entrynum && (thisentry = rawpack_readentry(infile)).valid) {
		thisentry.filename = basename(thisentry.filename); // For security reasons
		LOG("Checking if file should be extracted...");
		if (unpack_selection) {
			bool selected = false;
			for (char **filename = unpack_selection; filename != argv + argc; filename++) {
				if (strcmp(*filename, thisentry.filename) == 0) {
					selected = true;
					break;
				}
			}
			if (!selected) {
				continue;
			}
		}
		LOG("Opening output file: %s", thisentry.filename);
		outfile = fopen(thisentry.filename, "wb");
		if (!outfile) {
			perror(thisentry.filename);
			return EXIT_FAILURE;
		}
		LOG("Writing data...");
		fwrite(thisentry.data, sizeof(*thisentry.data), thisentry.data_len, outfile);
		LOG("Cleaning up...");
		fclose(outfile);
		rawpack_deleteentry(&thisentry);
	}
	if (!thisentry.valid) {
		ERROR("Unexpected end of file");
		return EXIT_FAILURE;
	}
	LOG("Succeeded");
}
