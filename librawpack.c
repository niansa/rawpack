/*
    rawpack
    Copyright (C) 2020 niansa

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "librawpack.h"

static const char rawpack_headerid[] = "rawpack";


bool rawpack_checkheader(FILE *thisfile) {
	// Read from file
	char headercmp[sizeof(rawpack_headerid)];
	fread(headercmp, sizeof(*rawpack_headerid), sizeof(rawpack_headerid), thisfile);
	// Compare
	return strncmp(headercmp, rawpack_headerid, sizeof(rawpack_headerid)) == 0;
}

unsigned int rawpack_readentrynum(FILE *thisfile) {
	unsigned int res;
	fread(&res, sizeof(res), 1, thisfile);
	return res;
}

struct rawpack_entry rawpack_readentry(FILE *thisfile) {
	struct rawpack_entry thisentry;
	thisentry.filename = NULL;
	size_t filename_len = 0;
	// Read filename
	getdelim(&thisentry.filename, &filename_len, 0, thisfile);
	// Read data len
	fread(&thisentry.data_len, sizeof(thisentry.data_len), 1, thisfile);
	// Check for EOF
	if ((thisentry.valid = feof(thisfile) == 0)) {
		// Create data buffer
		thisentry.data = malloc(thisentry.data_len);
		fread(thisentry.data, sizeof(*thisentry.data), thisentry.data_len, thisfile);
	}
	// Return result
	return thisentry;
}


void rawpack_writeheader(FILE *thisfile) {
	fwrite(rawpack_headerid, sizeof(*rawpack_headerid), sizeof(rawpack_headerid), thisfile);
}


void rawpack_writeentrynum(FILE *thisfile, unsigned int num) {
	fwrite(&num, sizeof(num), 1, thisfile);
}

void rawpack_writeentry(FILE *thisfile, struct rawpack_entry *thisentry) {
	fwrite(thisentry->filename, sizeof(*thisentry->filename), strlen(thisentry->filename) + 1, thisfile);
	fwrite(&thisentry->data_len, sizeof(thisentry->data_len), 1, thisfile);
	fwrite(thisentry->data, sizeof(*thisentry->data), thisentry->data_len, thisfile);
}


void rawpack_deleteentry(struct rawpack_entry *thisentry) {
	free(thisentry->filename);
	free(thisentry->data);
}
