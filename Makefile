#
#   rawpack
#   Copyright (C) 2020 niansa
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free Software
#   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
#   USA
#
EXTRAFLAGS=-O3

all: rawpack rawunpack

librawpack:
	gcc $(EXTRAFLAGS) librawpack.c -c -o obj/librawpack.o

rawpack: librawpack
	gcc $(EXTRAFLAGS) rawpack.c obj/librawpack.o -o bin/rawpack

rawunpack: librawpack
	gcc $(EXTRAFLAGS) rawunpack.c obj/librawpack.o -o bin/rawunpack

install:
	cp bin/* /usr/local/bin/

clean:
	rm -f obj/librawpack.o bin/rawpack bin/rawunpack
