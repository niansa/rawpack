/*
    rawpack
    Copyright (C) 2020 niansa

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <libgen.h>
#include "librawpack.h"
#include "termmagic.h"


long fgetsize(FILE *f) {
	long res;
	long bak;
	bak = ftell(f);
	fseek(f, 0L, SEEK_END);
	res = ftell(f);
	fseek(f, bak, SEEK_SET);
	return res;
}

int main(int argc, char **argv) {
        EL_PREPARE();
	FILE *outfile;
	FILE *infile;
	// Check args
	if (argc < 2) {
		ERROR("Bad usage");
		printf("Usage: %s <output> [input files ...]\n", argv[0]);
		return EXIT_FAILURE;
	}
	// Open output file
	LOG("Opening output file...");
	outfile = fopen(argv[1], "wb");
	if (!outfile) {
		perror(argv[1]);
		return EXIT_FAILURE;
	}
	// Prepare output file
	LOG("Preparing file...");
	rawpack_writeheader(outfile);
	rawpack_writeentrynum(outfile, argc - 2);
	// Iterate through input files
	struct rawpack_entry thisentry;
	for (char **arg = argv + 2; arg - argv != argc; arg++) {
		LOG("Opening input file: %s", *arg);
		infile = fopen(*arg, "rb");
		if (!infile) {
			perror(*arg);
			continue;
		}
		LOG("Preparing data structure...");
		thisentry.filename = basename(*arg);
		thisentry.data_len = fgetsize(infile);
		thisentry.data = malloc(thisentry.data_len);
		LOG("Reading file into memory...");
		fread(thisentry.data, sizeof(*thisentry.data), thisentry.data_len, infile);
		LOG("Feeding data into output file...");
		rawpack_writeentry(outfile, &thisentry);
		LOG("Cleaning up...");
		fclose(infile);
		free(thisentry.data);
	}
	LOG("Finished!");
}
