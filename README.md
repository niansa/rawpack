# rawpack
Rawpack is a simple yet probably far from perfect file archival format written in pure C for performance reasons

## Installation
1. `make` the targets you need. Default is both `rawpack` and `rawunpack`
3. `make install` as root, if you want

## Usage
Run without arguments to see usage.