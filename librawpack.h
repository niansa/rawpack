/*
    rawpack
    Copyright (C) 2020 niansa

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    USA
*/
#include <stdbool.h>


struct rawpack_entry {
	bool valid;
	char *filename; // 0-terminated
	ssize_t data_len; // length of data in chars
	char *data;
};

extern bool rawpack_checkheader(FILE *thisfile);
extern unsigned int rawpack_readentrynum(FILE *thisfile);
extern struct rawpack_entry rawpack_readentry(FILE *thisfile);

extern void rawpack_writeheader(FILE *thisfile);
extern void rawpack_writeentrynum(FILE *thisfile, unsigned int num);
extern void rawpack_writeentry(FILE *thisfile, struct rawpack_entry *thisentry);

extern void rawpack_deleteentry(struct rawpack_entry *thisentry);

